class DeliveryRule

  attr_reader :type, :cutoff_price, :charge, :predicate

  def initialize(type, cutoff_price, charge)
    @type = type
    @cutoff_price = cutoff_price
    @charge = charge

    if @type == 'less_than'
      @predicate = less_than_predicate
    elsif @type == 'more_than'
      @predicate = more_than_predicate
    end
  end

  def self.create_less_than(cutoff_price, charge)
    new('less_than', cutoff_price, charge)
  end

  def self.create_more_than(cutoff_price, charge)
    new('more_than', cutoff_price, charge)
  end

  private

  def less_than_predicate
    lambda do |total_price|
      return charge if total_price < cutoff_price
    end
  end

  def more_than_predicate
    lambda do |total_price|
      return charge if total_price > cutoff_price
    end
  end

end
