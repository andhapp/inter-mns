product_catalog = [
  Product.new('Jeans', 'J01', 32.95),
  Product.new('Blouse', 'B01', 24.95),
  Product.new('Socks', 'S01', 7.95)
]

delivery_rules = [
  DeliveryRule.create_less_than(50.00, 4.95),
  DeliveryRule.create_less_than(90.00, 2.95)
]

offer_rules = [
  OfferRule.create_buy_one_get_one_half_price('J01', 16.48)
]

describe Basket do

  let(:basket) { Basket.create(product_catalog, delivery_rules, offer_rules) }

  context '#add' do

    it 'should add products correctly' do
      basket.add('S01')

      expect(basket.products.size).to eq 1
      expect(basket.products.first).to eq('S01')
    end

  end

  context '#total' do

    describe 'S01, B01' do

      it 'should return correct total' do
        basket.add('S01')
        basket.add('B01')

        expect(basket.total).to eq(37.85)
      end

    end

    describe 'J01, B01' do

      it 'should return correct total' do
        basket.add('J01')
        basket.add('B01')

        expect(basket.total).to eq(60.85)
      end

    end

    describe 'J01, J01' do

      it 'should return correct total' do
        basket.add('J01')
        basket.add('J01')

        expect(basket.total).to eq(54.37)
      end

    end

    describe 'S01, S01, J01, J01, J01' do

      it 'should return correct total' do
        basket.add('S01')
        basket.add('S01')
        basket.add('J01')
        basket.add('J01')
        basket.add('J01')

        expect(basket.total).to eq(98.27)
      end

    end

  end

end
