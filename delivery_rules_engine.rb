class DeliveryRulesEngine

  NO_CHARGE = 0.0

  def self.evaluate(delivery_rules, total)
    delivery_rules.each do |delivery_rule|
      result = delivery_rule.predicate.call(total)
      return result if result
      next
    end

    return NO_CHARGE
  end

end
