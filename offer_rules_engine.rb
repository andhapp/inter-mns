class OfferRulesEngine

  NO_OFFER = 0.0

  def self.evaluate(offer_rules, products_count)
    offer_rules.each do |offer_rule|
      result = offer_rule.predicate.call(products_count)
      return result if result
      next
    end

    return NO_OFFER
  end

end
