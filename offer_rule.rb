class OfferRule

  attr_reader :product_code, :count_offset, :new_price, :predicate

  def initialize(product_code, count_offset, new_price)
    @product_code = product_code
    @count_offset = count_offset
    @new_price = new_price
    @predicate = lambda do |product_counts|
      if product_counts[@product_code] > 0 && product_counts[@product_code] >= count_offset
        return @new_price * (count_offset / 2)
      end
    end
  end

  def self.create_buy_one_get_one_half_price(product_code, new_price)
    count_offset = 2
    new(product_code, count_offset, new_price)
  end
end
