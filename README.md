# Venture Lab

## Prerequisites

* Ruby-2.4.0
* Rspec

## Run the specs

```
bundle exec rspec spec
```

## Design improvements

This is just a first cut implementation and could be further improved. Here are just some of them:

1) Not all the classes have unit test coverage. However, it sort of follows a black box testing strategy
where the actual basket interface is tested, but the internal classes it depends on aren't tested at a unit
test level.

2) A discount class for price and percentage will improve the overall design. These classes will hold the actual
discount. For example: the delivery charge should always be £4.95 and a discount should be applied depending on
the total basket price, so, for orders over £50.00 but less than £90.00 should apply a discount of £2.00.

3) Different types of delivery rules could also have different classes inheriting from DeliveryRule class, 
to hold what they actually stand for.

4) Different offer rules could just be different classes inheriting from a higher OfferRule class.

5) Basket should delegate the price calculation to a pricing engine as opposed to doing it within a basket. Basket should
just add products and know what's in the basket and let pricing engine co-ordinate with rules engine to calculate a final
price.

6) Rules could be more user-friendly to allow managers or product owners to set them up via a DSL.
