class Basket

  attr_reader :product_catalog, :delivery_rules, :offer_rules, :products

  def initialize(product_catalog, delivery_rules, offer_rules)
    @product_catalog = product_catalog
    @delivery_rules = delivery_rules
    @offer_rules = offer_rules
    @products = []
    @products_count = Hash.new(0)
  end

  def self.create(product_catalog, delivery_rules, offer_rules)
    new(product_catalog, delivery_rules, offer_rules)
  end

  def add(product_code)
    @products << product_code
    @products_count[product_code] += 1
  end

  def total
    add_delivery_charge(
      apply_offers(@products_count)
    )
  end

  private

  def apply_offers(products_count)
    offer_price = OfferRulesEngine.evaluate(offer_rules, products_count)
    products_total - offer_price
  end

  def products_total
    @products.inject(0.00) do |total, product_code|
      matched_product = product_catalog.find do |product|
        product.code == product_code
      end

      total + matched_product.price
    end.round(2)
  end

  def add_delivery_charge(products_total)
    delivery_price = DeliveryRulesEngine.evaluate(delivery_rules, products_total)
    (products_total + delivery_price).round(2)
  end

end
